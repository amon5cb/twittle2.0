from django.db import models
from datetime import datetime
from django.contrib import auth
from django.contrib.auth.models import User
from django.utils import timezone

User = auth.get_user_model()

# Create your models here.


class Profile(models.Model):
    id_user  = models.IntegerField( null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length = 20, blank=True)
    last_name = models.CharField(max_length = 20, blank=True)
    bio = models.TextField(blank=True)
    location = models.CharField(max_length=30, blank=True)
    profile_pic = models.ImageField(upload_to="profile_pics", blank=True)

    def __str__(self):
        return self.user.username
    


class Tweet(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.CharField(max_length=140)
    current_date = timezone.localtime(timezone.now())
    created_at = models.DateTimeField(max_length=140, default=current_date)
    def __str__(self):
        return self.text





# class for card that takes tweet as a foreign key and user as a foreign key and uses tweets text
# as the heading and the user's username as the body

# class Card(models.Model):
#     user = models.ForeignKey(User, on_delete=models.CASCADE)
#     tweet = models.ForeignKey(Tweet, on_delete=models.CASCADE)
#     created = models.DateField(default=timezone.now(), null=False, blank=False)

#     def __str__(self) -> str:
#         return f"{self.tweet}.. on {self.created}"
