# Generated by Django 4.2.2 on 2023-06-18 08:32

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("user", "0003_alter_tweet_created_at"),
    ]

    operations = [
        migrations.AlterField(
            model_name="tweet",
            name="created_at",
            field=models.DateTimeField(
                default=datetime.datetime(
                    2023, 6, 18, 8, 32, 42, 785082, tzinfo=datetime.timezone.utc
                ),
                max_length=140,
            ),
        ),
    ]
