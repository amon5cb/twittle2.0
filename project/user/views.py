
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from .forms import UserRegisterForm, UserProfileForm, TweetForm
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from .models import Tweet, Profile
from django.http import HttpResponse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView, View
from django.contrib import auth
from django.contrib.auth.models import User
from django.forms.models import model_to_dict
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

from collections import defaultdict

from itertools import chain



# Create your views here.
@login_required(login_url="login")
def index(request):

    
    
    if request.method == "POST":
        form = TweetForm(request.POST)
        if form.is_valid():
            user_model = User.objects.get(username=request.user)
            tweet = Tweet(
                user=user_model,
                text=form.cleaned_data.get("text"),
            )
            tweet.save()

            messages.success(request, f"Tweet created!")
            return redirect("index")
    else:
        form = TweetForm()
    
    fetch_tweets = Tweet.objects.all()
    my_profile = Profile.objects.filter(user=request.user)

    buddy_list = User.objects.all()


    return render(request, "user/index.html", {"form": form, "churps": fetch_tweets, "buddies": buddy_list, "my_profile": my_profile})


def register(request):
    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()

            username = form.cleaned_data.get("username")
            email = form.cleaned_data.get("email")
            
            # htmly = get_template('user/Email.html')
            # d = { 'username': username }
            # subject, from_email, to = 'welcome', 'amon5cb@gmail.com', email
            # html_content = htmly.render(d)
            # msg = EmailMultiAlternatives(subject, html_content, from_email, [to])
            # msg.attach_alternative(html_content, "text/html")
            # msg.send()
            messages.success(request, f"Account created for {username}!")



            #Create a profile for the user
                
            user_model = User.objects.get(username=username)
                
            profile = Profile.objects.create(
            user= user_model,
            id_user = user_model.id
            )              
            profile.save()

  
            user_login = authenticate(request, username=username, password=form.cleaned_data["password1"])
            login(request, user_login)
            
            # messages.success(request, f"Welcome {username}!")

            return redirect("index")
        
    else:
        form = UserRegisterForm()
    return render(request, "user/register.html", {"form": form, "title": "register"})


def Login(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")

            user = authenticate(request, username=username, password=password)

            if user is not None:
                form = login(request, user)
                messages.success(request, f"Welcome {username}!")   
                return redirect("index")
            else:
                messages.info(request, "Username or password is incorrect")
    
    form = AuthenticationForm()
    return render(request, "user/login.html", {"form": form, "title": "login"})




@login_required(login_url="login")
def profile(request):
    user_object = User.objects.get(username=request.user)

    user_profile = Profile.objects.get(user=user_object)
    
    fetch_tweets = Tweet.objects.filter(user=user_object)
    length_tweets = len(fetch_tweets)
    # User = auth.get_user_model()

 
        
    return render(request, "user/profile.html", {"title": "profile", "user_profile": user_profile, "churps": fetch_tweets, "length_tweets": length_tweets})



@login_required(login_url="login")
def update_profile(request):
    user_object = User.objects.get(username=request.user)
    fetch_tweets = Tweet.objects.filter(user=user_object)
    length_tweets = len(fetch_tweets)
    profile = Profile.objects.get(user=user_object)

    if request.method == "POST":
        form = UserProfileForm(request.POST)
        if form.is_valid():
           # update user profile

            first_name = form.cleaned_data.get("first_name")
            last_name = form.cleaned_data.get("last_name")
            location = form.cleaned_data.get("location")
            bio = form.cleaned_data.get("bio")

            profile.first_name = first_name
            profile.last_name = last_name
            profile.location = location
            profile.bio = bio
            
            profile.user.save()

         
            messages.success(request, f"Account updated!")
            return redirect("profile")
    else:
        form = UserProfileForm()

    # User = auth.get_user_model()
        
    return render(request, "user/update_profile.html", {"form":form, "title": "profile", "churps": fetch_tweets, "profile": profile})



@login_required(login_url="login")
def create(request):
    fetch_tweets = Tweet.objects.all()

    if request.method == "POST":
        form = TweetForm(request.POST)
        if form.is_valid():
            tweet = form.save(request)
            messages.success(request, f"Created tweet {tweet.id}!")
            return redirect("create")
    else:
        form = TweetForm()
    return render(request, "user/create.html", {"tweets": fetch_tweets,"form": form, "title": "create"})






class TweetListView(View):
    # model = Tweet
    template_name = "user/index.html"
    
    def get(self, request):
        tweets = Tweet.objects.all().order_by("created_at")
        
        return render(request, self.template_name, {"tweets": tweets})
    
    
    # context_object_name = "tweets"
    # ordering = ["-created"]
    # paginate_by = 5
    

# class CardView(View):
#     template_name = "user/index.html"
    
#     def get(self, request):

#         if request.method == "POST":
#             form = TweetForm(request.POST)
#             if form.is_valid():
#                 tweet = form.save(request)
#                 messages.success(request, f"Created tweet {tweet.id}!")
#                 return redirect("index")
#         else:
#             form = TweetForm()
        
        
#             cards = Card.objects.all().order_by("created_at").values()


#             qs = defaultdict(list)
#             for card in cards:
#                 qs[card['user']].append(card)
#                 qs[card['tweet']].append(card)
#                 qs[card['created']].append(card)


#             return render(request, self.template_name, {"form":form, "churps": cards})