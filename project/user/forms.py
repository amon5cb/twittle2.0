
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Tweet
from django import forms
from django.db import models
from django.contrib import auth
from django.contrib.auth.models import User

class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()
    phone_no = forms.CharField(max_length = 20)
    first_name = forms.CharField(max_length = 20)
    last_name = forms.CharField(max_length = 20)

    class Meta:
        model= User
        fields = ["username", "email", "password1", "password2"]


class TweetForm(forms.Form):
    text = forms.CharField(max_length=500, widget=forms.Textarea(attrs={"rows": 5, "class": "form-control", "placeholder": "What's happening?", "id":"tweet-text" }))
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
   
        self.fields['text'].label = False

    class Meta:
        model= Tweet
        fields = ["text"]
    
    



class UserProfileForm(forms.Form):

    first_name = forms.CharField(max_length = 20)
    last_name = forms.CharField(max_length = 20)
    bio = forms.CharField(max_length=500, widget=forms.Textarea(attrs={"rows": 5, "class": "form-control", "placeholder": "Bio", "id":"bio" }))
    location = forms.CharField(max_length=30, widget=forms.TextInput(attrs={"class": "form-control", "placeholder": "Location", "id":"location" }))

    
    class Meta:
        model= User
        fields = ["username", "email"]